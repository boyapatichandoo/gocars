package gocars

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/boyapatichandoo/goutil"
)

type car struct {
	Name    string `json:"name"`
	Company string `json:"company"`
}

var carsData = []car{{Name: "XC60", Company: "Volvo"}, {Name: "XC90", Company: "Volvo"}}

//GetCars ...
func GetCars(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	cars, err := json.Marshal(goutil.StringSort(carsData, func(i, j int) bool {
		return carsData[i].Name > carsData[j].Name
	}))
	if err != nil {
		log.Println("cars", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Write(cars)
}
